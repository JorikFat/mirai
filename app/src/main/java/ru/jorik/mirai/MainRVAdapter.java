package ru.jorik.mirai;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class MainRVAdapter extends RecyclerView.Adapter {

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 0;
    }

    public static class Line{
        TextView num;
        TextView text;
        TextView time;

        public Line (View view){
            num = (TextView) view.findViewById(R.id.lstItm_tv_num);
            text = (TextView) view.findViewById(R.id.lstItm_tv_text);
            time = (TextView) view.findViewById(R.id.lstItm_tv_time);
        }
    }
}
