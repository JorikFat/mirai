package ru.jorik.mirai;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import ru.jorik.mirai.model.Request;

public class NavMenu extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    static List<Request> requests;
    static RequestListAdapter arrayAdapterRequest; //переделать в list

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar2);
        setSupportActionBar(toolbar);

        requests = new ArrayList<>();

        ListView listView = (ListView) findViewById(R.id.main_lv_list);
//        ArrayAdapter rla = new RequestListAdapter(this, TempClass.tempRequestsArray);
//        arrayAdapterRequest = new RequestListAdapter(this, requests.toArray(new Request[requests.size()]));
        arrayAdapterRequest = new RequestListAdapter(this, requests);
        listView.setAdapter(arrayAdapterRequest);

//        ArrayAdapter<String> aa = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, tempStrings);
//        listView.setAdapter(aa);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout1);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout1);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main2, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.tempMethod) {
            addTempRequets();
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout1);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == 1) {
                Request aRequest;

                String name = data.getStringExtra(AddRequestActivity.NAME);
                double budjet = data.getDoubleExtra(AddRequestActivity.BUDJET, 0);
                Date date;
                try {
                    SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yy / HH:mm");
                    String tDate = data.getStringExtra(AddRequestActivity.DATE);
                    date = dateFormat.parse(tDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                    Log.e("CreateRequestError", "Ошибка формирования даты");
                    date = Calendar.getInstance().getTime();
                }
                String fio = data.getStringExtra(AddRequestActivity.FIO);
                String description = data.getStringExtra(AddRequestActivity.DESCRIPTION);

                aRequest = new Request(name, budjet, date, fio, description);
                arrayAdapterRequest.add(aRequest);
                arrayAdapterRequest.notifyDataSetChanged();
            }
        }
    }

    public void clickOnAddingPanel(View v){
        int id = v.getId();
        switch (id) {
            case R.id.adPnl_iv_request:
                startActivityForResult(new Intent(this, AddRequestActivity.class), 1);
                break;
            case R.id.adPnl_iv_contract:
                startActivity(new Intent(this, AddContractActivity.class));
                break;
            case R.id.adPnl_iv_client:
                startActivity(new Intent(this, AddClientActivity.class));
                break;
        }
    }

    public void clickOnGroupPanel(View v){
        int id = v.getId();
        uncheckGroupPanel();
        switch (id) {
            case R.id.grPl_RL_home:
                checkGroupPanel(1);
                showHomeLayout();
                break;
            case R.id.grPl_RL_request:
                checkGroupPanel(2);
                showList(2);
                break;
            case R.id.grPl_RL_add:
                showCloseAddingPanel();
                break;
            case R.id.grPl_RL_contract:
                checkGroupPanel(4);
                showList(4);
                break;
            case R.id.grPl_RL_client:
                checkGroupPanel(5);
                showList(5);
                break;
        }
    }

    private void showHomeLayout(){
        findViewById(R.id.main_lv_list).setVisibility(View.GONE);
        findViewById(R.id.include4).setVisibility(View.VISIBLE);
    }

    private void showList(int index){
        ArrayAdapter aa;
        findViewById(R.id.include4).setVisibility(View.GONE);
        ListView listView = (ListView) findViewById(R.id.main_lv_list);
        listView.setVisibility(View.VISIBLE);
        switch (index){
            case 2:
//                aa = new RequestListAdapter(this, TempClass.tempRequestsArray);
                aa = arrayAdapterRequest;
                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        startActivity(new Intent(NavMenu.this, RequestDetailActivity.class));
                    }
                });
                break;
            case 4:
                aa = new ContractListAdapter(this, TempClass.tempContractArray);
                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        startActivity(new Intent(NavMenu.this, ContractDetailActivity.class));
                    }
                });
                break;
            case 5:
                aa = new ClientListAdapter(this, TempClass.tempClientsArray);
                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        startActivity(new Intent(NavMenu.this, ClientDeteilActivity.class));
                    }
                });
                break;
            default:
//                aa = new ArrayAdapter(this, android.R.layout.simple_list_item_1, TempClass.tempRequestsArray);
                aa = arrayAdapterRequest;
        }
        listView.setAdapter(aa);
    }

    private void showCloseAddingPanel(){
        View view = findViewById(R.id.addingLayout);
        if (view.getVisibility() == View.VISIBLE) {
            view.setVisibility(View.GONE);
        } else if (view.getVisibility() == View.GONE) {
            view.setVisibility(View.VISIBLE);
        }
    }

    private void uncheckGroupPanel(){
        ((ImageView) findViewById(R.id.grPl_iv_home)).setImageResource(R.drawable.main);
        ((ImageView) findViewById(R.id.grPl_iv_request)).setImageResource(R.drawable.application);
//        ((ImageView) findViewById(R.id.grPl_iv_add)).setImageResource(R.drawable.add);
        ((ImageView) findViewById(R.id.grPl_iv_contract)).setImageResource(R.drawable.contract);
        ((ImageView) findViewById(R.id.grPl_iv_client)).setImageResource(R.drawable.client);
    }

    private void checkGroupPanel(int index){
        switch (index){
            case 1:
                ((ImageView) findViewById(R.id.grPl_iv_home)).setImageResource(R.drawable.main_selected);
                break;
            case 2:
                ((ImageView) findViewById(R.id.grPl_iv_request)).setImageResource(R.drawable.application_selected);
                break;
/*
            case 3:
                ((ImageView) findViewById(R.id.grPl_iv_add)).setImageResource(R.drawable.add);
                break;
*/
            case 4:
                ((ImageView) findViewById(R.id.grPl_iv_contract)).setImageResource(R.drawable.contract_selected);
                break;
            case 5:
                ((ImageView) findViewById(R.id.grPl_iv_client)).setImageResource(R.drawable.client_selected);
                break;
        }
    }

    private void addTempRequets(){
        Request tRequest = new Request(1, "tempText", Calendar.getInstance().getTime());
        arrayAdapterRequest.add(tRequest);
        arrayAdapterRequest.notifyDataSetChanged();
    }

    public void showMessage (View v){
        Toast.makeText(this, String.valueOf(v.getId()), Toast.LENGTH_SHORT).show();
    }
}
