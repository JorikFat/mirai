package ru.jorik.mirai;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import ru.jorik.mirai.model.Contract;

/**
 * Created by 111 on 17.10.2017.
 */

public class ContractListAdapter extends ArrayAdapter<Contract> {
    Context context;
    Contract[] requests;

    public ContractListAdapter(@NonNull Context context, Contract[] values) {
        super(context, R.layout.list_item, values);
        this.context = context;
        requests = values;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.list_item, null, false);
        TextView num = (TextView) rowView.findViewById(R.id.lstItm_tv_num);
        TextView text = (TextView) rowView.findViewById(R.id.lstItm_tv_text);
        TextView time = (TextView) rowView.findViewById(R.id.lstItm_tv_time);

//        num.setText(String.valueOf(requests[position].num));
//        text.setText(requests[position].text);
//        time.setText(requests[position].time);

        return rowView;
    }
}
