package ru.jorik.mirai;

import java.util.Calendar;
import java.util.Date;

import ru.jorik.mirai.model.Client;
import ru.jorik.mirai.model.Contract;
import ru.jorik.mirai.model.Request;

/**
 * Created by 111 on 18.10.2017.
 */

public class TempClass {
    static Date tempDate = Calendar.getInstance().getTime();

    public static Request[] tempRequestsArray = {
            new Request(1, "Название заявки", tempDate),
            new Request(2, "Название заявки", tempDate),
            new Request(3, "Название заявки", tempDate),
            new Request(4, "Название заявки", tempDate),
            new Request(5, "Название заявки", tempDate),
            new Request(6, "Название заявки", tempDate),
            new Request(7, "Название заявки", tempDate),
            new Request(8, "Название заявки", tempDate),
            new Request(9, "Название заявки", tempDate),
            new Request(10, "Название заявки", tempDate),
            new Request(11, "Название заявки", tempDate)
    };


    public static Contract[] tempContractArray = {
            new Contract(1, "Предмет договора", "4:15"),
            new Contract(2, "Предмет договора", "4:15"),
            new Contract(3, "Предмет договора", "4:15"),
            new Contract(4, "Предмет договора", "4:15"),
            new Contract(5, "Предмет договора", "4:15"),
            new Contract(6, "Предмет договора", "4:15"),
            new Contract(7, "Предмет договора", "4:15"),
            new Contract(8, "Предмет договора", "4:15"),
            new Contract(9, "Предмет договора", "4:15"),
            new Contract(10, "Предмет договора", "4:15"),
            new Contract(11, "Предмет договора", "4:15")
    };
    public static Client[] tempClientsArray= {
            new Client(1, "ФИО клиента", "4:15"),
            new Client(2, "ФИО клиента", "4:15"),
            new Client(3, "ФИО клиента", "4:15"),
            new Client(4, "ФИО клиента", "4:15"),
            new Client(5, "ФИО клиента", "4:15"),
            new Client(6, "ФИО клиента", "4:15"),
            new Client(7, "ФИО клиента", "4:15"),
            new Client(8, "ФИО клиента", "4:15"),
            new Client(9, "ФИО клиента", "4:15"),
            new Client(10, "ФИО клиента", "4:15"),
            new Client(11, "ФИО клиента", "4:15")
    };

}
