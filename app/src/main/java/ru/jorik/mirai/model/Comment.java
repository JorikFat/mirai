package ru.jorik.mirai.model;

import java.util.Date;

/**
 * Created by 111 on 25.10.2017.
 */

public class Comment {
    int id;
    private Date time;
    private String author;
    private String text;

    public Comment(String author, Date time, String text) {
        this.time = time;
        this.author = author;
        this.text = text;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
