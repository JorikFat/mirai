package ru.jorik.mirai.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by 111 on 17.10.2017.
 */

public class Request {
    private int id;
    private String name;
    private double budjet;
    private Date time;
    private String fio;
    private String description;
    private List<Comment> comments;

    public Request(String name, double budjet, Date time, String fio, String description, List<Comment> comments) {
        this.name = name;
        this.budjet = budjet;
        this.time = time;
        this.fio = fio;
        this.description = description;
        this.comments = comments;
    }

    public Request(String name, double budjet, Date time, String fio, String description) {
        this.name = name;
        this.budjet = budjet;
        this.time = time;
        this.fio = fio;
        this.description = description;
        this.comments = new ArrayList<>();
    }

    public Request(int id, String name, Date time) {
        this.id = id;
        this.name = name;
        this.time = time;

        this.budjet = 0;
        this.fio = "Клиент не указан";
        this.description = "Описание не указано";
        this.comments = new ArrayList<>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getBudjet() {
        return budjet;
    }

    public void setBudjet(double budjet) {
        this.budjet = budjet;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public String getFio() {
        return fio;
    }

    public void setFio(String fio) {
        this.fio = fio;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }
}
