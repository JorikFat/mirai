package ru.jorik.mirai;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;

import java.util.ArrayList;
import java.util.List;

public class AddContractActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_contract);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        final List<String> listSting = new ArrayList<>();
        final ArrayAdapter arrayAdapter = new ArrayAdapter(this, R.layout.comment_layout, listSting);


/*
        Button button = (Button) findViewById(R.id.coms_btn_addComm);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(AddContractActivity.this, "123", Toast.LENGTH_SHORT).show();
                addComent(arrayAdapter, listSting);
            }
        });
*/
        findViewById(R.id.coms_tv_showAll).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAllComments(v);
            }
        });


        Button commentButton = (Button) findViewById(R.id.coms_btn_addComm);
        commentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAndGone();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.add_req, menu);
        return super.onCreateOptionsMenu(menu);
    }

    public void addComent(ArrayAdapter aa, List<String> ls){
        ls.add("Строка");
        aa.notifyDataSetChanged();
    }

    public void showAndGone(){
        View commentLine = findViewById(R.id.inputCommentLine);
        if (commentLine.getVisibility() == View.VISIBLE){
            commentLine.setVisibility(View.GONE);
        } else if (commentLine.getVisibility() == View.GONE) {
            commentLine.setVisibility(View.VISIBLE);
        }
    }

    public void showAllComments(View v){
        findViewById(R.id.hideComment1).setVisibility(View.VISIBLE);
        findViewById(R.id.hideComment2).setVisibility(View.VISIBLE);
        findViewById(R.id.hideComment3).setVisibility(View.VISIBLE);
    }

}
