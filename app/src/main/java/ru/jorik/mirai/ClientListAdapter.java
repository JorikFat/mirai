package ru.jorik.mirai;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.Calendar;
import java.util.Date;

import ru.jorik.mirai.model.Client;

/**
 * Created by 111 on 17.10.2017.
 */

public class ClientListAdapter extends ArrayAdapter<Client> {
    Context context;
    Client[] requests;

    public ClientListAdapter(@NonNull Context context, Client[] values) {
        super(context, R.layout.list_item, values);
        this.context = context;
        requests = values;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.list_item, null, false);
        TextView num = (TextView) rowView.findViewById(R.id.lstItm_tv_num);
        TextView text = (TextView) rowView.findViewById(R.id.lstItm_tv_text);
        TextView time = (TextView) rowView.findViewById(R.id.lstItm_tv_time);

//        num.setText(String.valueOf(requests[position].num));
//        text.setText(requests[position].text);
//        time.setText(getTime(requests[position].getTime));

        return rowView;
    }

    private String getTime(Date date){
        String rString= "%d:%d";
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);
        String.format(rString, hour, minute);
        return rString;
    }
}
