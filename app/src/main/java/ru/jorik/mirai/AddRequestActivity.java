package ru.jorik.mirai;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import ru.jorik.mirai.model.Comment;
import ru.jorik.mirai.model.Request;

public class AddRequestActivity extends AppCompatActivity {

    public static final String NAME = "name";
    public static final String BUDJET = "budjet";
    public static final String DATE = "date";
    public static final String FIO = "fio";
    public static final String DESCRIPTION = "description";

    EditText nameField;
    EditText budjetField;
    EditText dateField;
    EditText fioField;
    EditText descriptionField;
    Request rRequest;
    Intent rIntent;
    SimpleDateFormat dateFormat;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_request);
        initFields();
        initView();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        findViewById(R.id.coms_tv_showAll).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAllComments(v);
            }
        });

        Button commentButton = (Button) findViewById(R.id.coms_btn_addComm);
        commentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAndGone();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.add_req, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        createRequest();
        putExtraInIntent();
        setResult(RESULT_OK, rIntent);
        finish();
        return super.onOptionsItemSelected(item);
    }

    public void showAndGone(){
        View commentLine = findViewById(R.id.inputCommentLine);
        if (commentLine.getVisibility() == View.VISIBLE){
            commentLine.setVisibility(View.GONE);
        } else if (commentLine.getVisibility() == View.GONE) {
            commentLine.setVisibility(View.VISIBLE);
        }
    }

    public void showAllComments(View v){
        findViewById(R.id.hideComment1).setVisibility(View.VISIBLE);
        findViewById(R.id.hideComment2).setVisibility(View.VISIBLE);
        findViewById(R.id.hideComment3).setVisibility(View.VISIBLE);
    }

    private void initFields(){
        rIntent = new Intent();
        dateFormat = new SimpleDateFormat("dd.MM.yy / HH:mm");
    }

    private void initView(){
        nameField = (EditText) findViewById(R.id.addRq_et_name);
        budjetField = (EditText) findViewById(R.id.addRq_et_bujet);
        dateField = (EditText) findViewById(R.id.addRq_et_date);
        fioField = (EditText) findViewById(R.id.addRq_et_FIO);
        descriptionField = (EditText) findViewById(R.id.addRq_et_description);
    }

    private void createRequest(){//todo сократить этот метод
        String name;
        double budjet;
        Date time;
        String fio;
        String description;


        name = nameField.getText().toString();
        try{
            budjet = Double.parseDouble(budjetField.getText().toString());
        } catch (NumberFormatException pex){
            budjet = 0;
        }
        try{
            time = dateFormat.parse(dateField.getText().toString());
        } catch (ParseException pex){
            pex.printStackTrace();
            Log.e("AddRequestActError", "Ошибка преобразования текста в Date", pex);
            time = Calendar.getInstance().getTime();
        }
        fio = fioField.getText().toString();
        description = descriptionField.getText().toString();


        rRequest = new Request(name, budjet, time, fio, description, new ArrayList<Comment>());
    }

    private void putExtraInIntent(){
        String name = rRequest.getName();
        double budjet = rRequest.getBudjet();
//        Date time = rRequest.getTime();//проблематично передать
        String timeS = dateFormat.format(rRequest.getTime());
        String fio = rRequest.getFio();
        String description = rRequest.getDescription();

        rIntent.putExtra(NAME, name);
        rIntent.putExtra(BUDJET, budjet);
//        rIntent.putExtra(DATE, time);//проблематично передать
        rIntent.putExtra(DATE, timeS);
        rIntent.putExtra(FIO, fio);
        rIntent.putExtra(DESCRIPTION, description);
    }
}
